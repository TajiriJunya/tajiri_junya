package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/*")
public class LoginFilter implements Filter {
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession();
		HttpServletResponse hresponse = (HttpServletResponse)response;
		HttpServletRequest hrequest = (HttpServletRequest)request;
		List<String> errorMessages = new ArrayList<String>();

		if(!(hrequest.getServletPath().equals("/login")) && !(hrequest.getServletPath().contains(".css"))) {
			if (session.getAttribute("loginUser") == null) {
				errorMessages.add("ログインを行ってください");
				session.setAttribute("errorMessages",errorMessages);
				hresponse.sendRedirect("login");
			}else {
				chain.doFilter(request, response); // サーブレットを実行
			}
		}else {
			chain.doFilter(request, response); // サーブレットを実行
		}
	}

	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO 自動生成されたメソッド・スタブ

	}

}
