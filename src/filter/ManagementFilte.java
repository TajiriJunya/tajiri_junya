package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns = {"/management","/setting","/signup"})
public class ManagementFilte implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession();
		User loginUser = (User) session.getAttribute("loginUser");
		HttpServletResponse hresponse = (HttpServletResponse)response;
		List<String> errorMessages = new ArrayList<String>();

		if (loginUser.getDepartment() != 1 && loginUser.getBranch() != 1) {
			errorMessages.add("ログイン中のアカウントに管理者権限がありません");
			session.setAttribute("errorMessages",errorMessages);
			hresponse.sendRedirect("./");
		}else {
			chain.doFilter(request, response); // サーブレットを実行
		}
	}

	@Override
	public void destroy() {

	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO 自動生成されたメソッド・スタブ
	}

}
