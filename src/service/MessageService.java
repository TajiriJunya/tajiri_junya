package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.mysql.jdbc.StringUtils;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public List<UserMessage> select(String startDate, String endDate, String categoryWord) {
		final int LIMIT_NUM = 1000;
		Date date =new Date();
		Connection connection = null;
		try {
			connection = getConnection();
			if(StringUtils.isEmptyOrWhitespaceOnly(startDate)) {
			startDate = "2020-01-01 00:00:00";
			}else {
			startDate = startDate + " 00:00:00";
			}

			if(StringUtils.isEmptyOrWhitespaceOnly(endDate)) {
				endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
			}else {
				endDate = endDate +" 23:59:59";
			}

			if(StringUtils.isEmptyOrWhitespaceOnly(categoryWord)) {
				categoryWord = null;
			}
			List<UserMessage> messages = new UserMessageDao().select(connection, LIMIT_NUM,startDate,endDate,categoryWord);
			commit(connection);
			return messages;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}


	public void delete(int message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}