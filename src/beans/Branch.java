package beans;

import java.io.Serializable;

public class Branch implements Serializable {


	private String branch;
	private int branchId;

	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public int getBranchId() {
		return branchId;
	}
	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}
}