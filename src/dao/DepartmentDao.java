package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Department;
import exception.SQLRuntimeException;

public class DepartmentDao {

	public List<Department> select(Connection connection) {

		PreparedStatement ps = null;
		try {

			StringBuilder sql = new StringBuilder();


			sql.append("SELECT ");
			sql.append("    id as departmentid, ");
			sql.append("    name as department ");
			sql.append("FROM departments ");
			sql.append("ORDER BY id ASC " );

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<Department> departments = toUserMessages(rs);
			return departments;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Department> toUserMessages(ResultSet rs) throws SQLException {

		List<Department> userlists = new ArrayList<Department>();
		try {
			while (rs.next()) {
				Department user = new Department();

				user.setDepartment(rs.getString("department"));
				user.setDepartmentId(rs.getInt("departmentid"));
				userlists.add(user);
			}
			return userlists;
		} finally {
			close(rs);
		}
	}
}
