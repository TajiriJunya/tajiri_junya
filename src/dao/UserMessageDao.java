package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserMessage;
import exception.SQLRuntimeException;


public class UserMessageDao {

	public List<UserMessage> select(Connection connection, int limit, String startDate,String endDate, String categoryWord) {

		PreparedStatement ps = null;
		try {

			StringBuilder sql = new StringBuilder();

			sql.append("SELECT ");
			sql.append("    messages.id as id, ");
			sql.append("    messages.title as title, ");
			sql.append("    messages.text as text, ");
			sql.append("    users.account as account, ");
			sql.append("    users.name as name, ");
			sql.append("    users.id as userId, ");
			sql.append("    messages.category as category, ");
			sql.append("    messages.updated_date as updated_date, ");
			sql.append("    messages.created_date as created_date  ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id    ");
			sql.append("WHERE messages.created_date >=  ? ");
			sql.append("AND messages.created_date   <   ? ");
			if(categoryWord !=null) {
			sql.append("AND messages.category Like ? ");
			}
			sql.append("ORDER BY created_date DESC limit " + limit);

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, startDate);
			ps.setString(2, endDate);
			if(categoryWord !=null) {
			ps.setString(3, categoryWord);
			}

			ResultSet rs = ps.executeQuery();

			List<UserMessage> messages = toUserMessages(rs);
			return messages;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {

		List<UserMessage> messages = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				UserMessage message = new UserMessage();
				message.setId(rs.getInt("id"));
				message.setText(rs.getString("text"));
				message.setAccount(rs.getString("account"));
				message.setTitle(rs.getString("title"));
				message.setCategory(rs.getString("category"));
				message.setName(rs.getString("name"));
				message.setUserId(rs.getInt("userId"));
				message.setCreatedDate(rs.getTimestamp("created_date"));
				message.setUpdatedDate(rs.getTimestamp("updated_date"));

				messages.add(message);
			}
			return messages;
		} finally {
			close(rs);
		}
	}
}