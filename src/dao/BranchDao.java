package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import exception.SQLRuntimeException;


public class BranchDao {

	public List<Branch> select(Connection connection) {

		PreparedStatement ps = null;
		try {

			StringBuilder sql = new StringBuilder();

			sql.append("SELECT ");
			sql.append("    id as branchid, ");
			sql.append("    name as branch ");
			sql.append("FROM branches ");
			sql.append("ORDER BY id ASC ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<Branch> branches = toUserMessages(rs);
			return branches;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Branch> toUserMessages(ResultSet rs) throws SQLException {

		List<Branch> userlists = new ArrayList<Branch>();
		try {
			while (rs.next()) {
				Branch user = new Branch();

				user.setBranch(rs.getString("branch"));
				user.setBranchId(rs.getInt("branchid"));
				userlists.add(user);
			}
			return userlists;
		} finally {
			close(rs);
		}
	}
}
