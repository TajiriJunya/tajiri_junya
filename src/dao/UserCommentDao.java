package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import exception.SQLRuntimeException;


public class UserCommentDao {

	public List<beans.UserComment> select(Connection connection) {

		PreparedStatement ps = null;
		try {

			StringBuilder sql = new StringBuilder();

			sql.append("SELECT ");
			sql.append("    users.account as account, ");
			sql.append("    users.name as name, ");
			sql.append("    comments.id as id, ");
			sql.append("    comments.text as text, ");
			sql.append("    comments.created_date as created_date, ");
			sql.append("    comments.user_id as user_id, ");
			sql.append("    comments.message_id as message_id ");
			sql.append("FROM users ");
			sql.append("INNER JOIN comments ");
			sql.append("ON users.id = comments.user_id ");
			sql.append("ORDER BY comments.created_date ASC " );


			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();

			List<beans.UserComment> users = toUsers(rs);

			return users;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<beans.UserComment> toUsers(ResultSet rs) throws SQLException {

		List<beans.UserComment> userlists = new ArrayList<beans.UserComment>();
		try {
			while (rs.next()) {
				beans.UserComment user = new beans.UserComment();

				user.setmessageId(rs.getInt("message_id"));
				user.setUserId(rs.getInt("user_id"));
				user.setAccount(rs.getString("account"));
				user.setUserName(rs.getString("name"));
				user.setId(rs.getInt("id"));
				user.setText(rs.getString("text"));
				user.setCreatedDate(rs.getTimestamp("created_date"));

				userlists.add(user);

			}
			return userlists;
		} finally {
			close(rs);
		}
	}
}
