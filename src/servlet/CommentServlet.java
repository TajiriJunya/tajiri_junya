package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		String text = request.getParameter("text");

		if (!isValid(text, errorMessages)) {
			session.setAttribute("errorMessages",errorMessages);
			response.sendRedirect("./");
			return;
		}

		Comment comment = new Comment();
		User user = (User) session.getAttribute("loginUser");

		comment.setText(text);
		comment.setUserId(user.getId());
		comment.setmessageId(Integer.parseInt(request.getParameter("messageId")));

		session.setAttribute("errorMessages",null);
		new CommentService().insert(comment);
		response.sendRedirect("./");
	}

	private boolean isValid(String text, List<String> errorMessages) {

		if (StringUtils.isEmptyOrWhitespaceOnly(text)) {
			errorMessages.add("コメントを入力してください");
		}
		if (500 < text.length()) {
			errorMessages.add("コメントは500文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}

