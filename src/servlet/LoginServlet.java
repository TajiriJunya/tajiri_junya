package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		request.setAttribute("errorMessages",session.getAttribute("errorMessages"));
		request.getRequestDispatcher("login.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String account = request.getParameter("account");
		String password = request.getParameter("password");
		List<String> errorMessages = new ArrayList<String>();
		User user = new UserService().select(account, password);
		HttpSession session = request.getSession();

		if(!isValid(account, password, errorMessages)){
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		if (user == null || user.getIsStopped() == 1) {
			errorMessages.add("アカウント名またはパスワードが誤っています");
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("inputUser",account);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		session.setAttribute("loginUser", user);
		response.sendRedirect("./");
	}

	private boolean isValid(String account,String password, List<String> errorMessages) {


		if (StringUtils.isEmptyOrWhitespaceOnly(account)) {
			errorMessages.add("アカウント名を入力してください");
		}

		if (StringUtils.isEmptyOrWhitespaceOnly(password)) {
			errorMessages.add("パスワードを入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}