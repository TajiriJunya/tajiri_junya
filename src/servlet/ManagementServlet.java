package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import beans.UserBranchDepartment;
import service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<UserBranchDepartment> accounts = new UserService().list();
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		request.setAttribute("loginUserId",loginUser.getId());
		request.setAttribute("accounts", accounts);
		request.getRequestDispatcher("management.jsp").forward(request, response);

		session.setAttribute("errorMessages",null);
	}

}