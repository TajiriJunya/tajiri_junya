package servlet;

import java.io.IOException;
//import java.util.List;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<UserMessage> messages = new MessageService().select(request.getParameter("start"),request.getParameter("end"),request.getParameter("categoryWord"));
		List<UserComment> comments = new CommentService().select();
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		request.setAttribute("loginUserId",loginUser.getId());
		request.setAttribute("errorMessages",session.getAttribute("errorMessages"));
		request.setAttribute("start",request.getParameter("start"));
		request.setAttribute("end", request.getParameter("end"));
		request.setAttribute("categoryWord", request.getParameter("categoryWord"));
		request.setAttribute("filterDepartment", loginUser.getDepartment());
		request.setAttribute("filterBranch", loginUser.getBranch());
		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);
		request.getRequestDispatcher("home.jsp").forward(request, response);

		session.setAttribute("errorMessages",null);
	}

}