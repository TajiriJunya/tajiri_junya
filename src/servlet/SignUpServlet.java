package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<Department> departments = new DepartmentService().select();
		List<Branch> branches = new BranchService().select();

		request.setAttribute("departments", departments);
		request.setAttribute("branches", branches);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();
		List<Department> departments = new DepartmentService().select();
		List<Branch> branches = new BranchService().select();

		User user = getUser(request);

		if (!isValid(user, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("departments", departments);
			request.setAttribute("branches", branches);
			request.setAttribute("user", user);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}

		new UserService().insert(user);
		response.sendRedirect("./");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setRepassword(request.getParameter("repassword"));
		user.setName(request.getParameter("name"));
		user.setBranch(Integer.parseInt(request.getParameter("branchid")));
		user.setDepartment(Integer.parseInt(request.getParameter("departmentid")));
		return user;
	}

	private boolean isValid(User user, List<String> errorMessages) {

		User user1 = new UserService().select(user.getAccount());

		String name = user.getName();
		String account = user.getAccount();
		String password = user.getPassword();
		String repassword = user.getRepassword();
		int branch = user.getBranch();
		int department = user.getDepartment();

		if (user1 != null) {
			errorMessages.add("アカウントが重複しています");
		}

		if (account.length() < 6 ) {
			errorMessages.add("アカウント名は6文字以上で入力してください");
		}else if (20 < account.length()) {
			errorMessages.add("アカウント名は20文字以下で入力してください");
		}

		if (!account.matches("^[0-9a-zA-Z]*$")) {
			errorMessages.add("アカウント名は半角英数字で入力してください");
		}

		if (StringUtils.isEmptyOrWhitespaceOnly(password)) {
			errorMessages.add("パスワードを入力してください");
		}else if(!password.equals(repassword)) {
			errorMessages.add("パスワードが確認用パスワードと一致しません");
		}

		if ((!password.matches("^[-_@+*;:#$%&A-Za-z0-9]+$")) && ((6 > password.length()) || (20 < password.length()))) {
			errorMessages.add("パスワードは記号を含むすべての半角文字で6文字以上20文字以下で設定してください");
		}

		if (StringUtils.isEmptyOrWhitespaceOnly(name)){
			errorMessages.add("名前を入力してください");
		}

		if (10 < name.length()) {
			errorMessages.add("名前は10文字以下で入力してください");
		}

		if((branch != 1 && (department == 1 || department == 2))){
			errorMessages.add("部署と支社が一致しません");
		}
		if((branch == 1 && (department == 3 || department == 4))){
			errorMessages.add("部署と支社が一致しません");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}