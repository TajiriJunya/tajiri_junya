package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		if(!urlIsValid(errorMessages,request)){
			session.setAttribute("errorMessages",errorMessages);
			response.sendRedirect("management");
			return;
		}

		User user = new UserService().select(Integer.parseInt(request.getParameter("userId")));
		List<Department> departments = new DepartmentService().select();
		List<Branch> branches = new BranchService().select();
		User logoinUser = (User) session.getAttribute("loginUser");

		request.setAttribute("logoinUser", logoinUser);
		request.setAttribute("departments", departments);
		request.setAttribute("branches", branches);
		request.setAttribute("user", user);
		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{

		List<String> errorMessages = new ArrayList<String>();
		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();
		HttpSession session = request.getSession();
		User logoinUser = (User) session.getAttribute("loginUser");

		User updatedUser = getUser(request);
	//	String repass = request.getParameter("repassword");

		if(!isValid(updatedUser, errorMessages,request)) {
			request.setAttribute("logoinUser", logoinUser);
			request.setAttribute("user", updatedUser);
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}

		new UserService().update(updatedUser);
		response.sendRedirect("management");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setRepassword(request.getParameter("repassword"));
		user.setName(request.getParameter("name"));
		user.setBranch(Integer.parseInt(request.getParameter("branchid")));
		user.setDepartment(Integer.parseInt(request.getParameter("departmentid")));
		user.setId(Integer.parseInt(request.getParameter("userId")));
		return user;
	}

	private boolean isValid(User user, List<String> errorMessages, HttpServletRequest request) {

		User user1 = new UserService().select(user.getAccount());
		User user2 = new UserService().select(Integer.parseInt(request.getParameter("userId")));

		String name = user.getName();
		String account = user.getAccount();
		String password = user.getPassword();
		String repassword = user.getRepassword();
		int branch = user.getBranch();
		int department = user.getDepartment();
		String accountname = user2.getAccount();

		if (user1 != null && !(accountname.equals(account))) {
			errorMessages.add("アカウントが重複しています");
		}

		if (account.length() < 6 ) {
			errorMessages.add("アカウント名は6文字以上で入力してください");
		}else if (20 <= account.length()) {
			errorMessages.add("アカウント名は20文字以下で入力してください");
		}

		if (!account.matches("^[0-9a-zA-Z]*$")) {
		errorMessages.add("アカウント名は半角英数字で入力してください");
		}

		if (StringUtils.isEmptyOrWhitespaceOnly(password)) {
			errorMessages.add("パスワードを入力してください");
		}else if(!password.equals(repassword)) {
			errorMessages.add("パスワードが確認用パスワードと一致しません");
		}

		if ((!password.matches("^[-_@+*;:#$%&A-Za-z0-9]+$")) && ((6 > password.length()) || (20 < password.length()))) {
			errorMessages.add("パスワードは記号を含むすべての半角文字で6文字以上20文字以下で設定してください");
		}

		if (StringUtils.isEmptyOrWhitespaceOnly(name)){
			errorMessages.add("名前を入力してください");
		}

		if (10 < name.length()) {
			errorMessages.add("名前は10文字以下で入力してください");
		}

		if((branch != 1 && (department == 1 || department == 2))){
			errorMessages.add("部署と支社が一致しません");
		}
		if((branch == 1 && (department == 3 || department == 4))){
			errorMessages.add("部署と支社が一致しません");
		}


		if (errorMessages.size() != 0) {
			return false;
		}

		return true;
	}

	private boolean urlIsValid( List<String> errorMessages,HttpServletRequest request) {

		String userId = request.getParameter("userId");
		String regex_num = "^[0-9]+$" ;
		Pattern p1 = Pattern.compile(regex_num);
		Matcher m1 = p1.matcher(userId);
		boolean result1 = m1.matches();


		if((!result1) || userId == null) {
			errorMessages.add("不正なパラメータが入力されました");
		}else if(new UserService().select(Integer.parseInt(request.getParameter("userId"))) == null) {
			errorMessages.add("不正なパラメータが入力されました");
		}


		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
