package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/deletemessage" })
public class DeleteMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		new MessageService().delete(Integer.parseInt(request.getParameter("messageId")));
		new CommentService().deleteMassageComment(Integer.parseInt(request.getParameter("messageId")));

		response.sendRedirect("./");

	}
}