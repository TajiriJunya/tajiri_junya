<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>${user.name}の設定</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
			</c:if>

			<form action="setting" method="post"><br />

				<label for="account">アカウント</label>
				<input name="account" id="account" value="${user.account}"/> <br />

				<label for="password">パスワード</label>
				<input name="password" type="password" id="password" /> <br />

				<label for="repassword">確認用パスワード</label>
				<input name="repassword" type="password" id="repassword" /> <br />

				<label for="name">名前</label>
				<input name="name" id="name" value="${user.name}"/><br />

				<div class="pulldownset">
					<c:if test="${ loginUser.id == user.id }">
						支社名
						<select class="branches" name="branchid" id="branch">
							<c:forEach items="${branches}" var="branch">
								<c:if test= "${branch.branchId == user.branch}">
									<option value="${branch.branchId}"selected>${branch.branch}</option>
								</c:if>
							</c:forEach>
						</select><br />
						部署名
						<select class="departments" name="departmentid" id="department">
							<c:forEach items="${departments}" var="department">
								<c:if test= "${department.departmentId == user.department}">
									<option value="${department.departmentId}"selected>${department.department}</option>
								</c:if>
							</c:forEach>
						</select><br />
					</c:if>
					<c:if test="${ loginUser.id != user.id }">
						支社名
						<select class="branches" name="branchid" id="branch">
							<c:forEach items="${branches}" var="branch">
								<c:if test= "${branch.branchId == user.branch}">
									<option value="${branch.branchId}"selected>${branch.branch}</option>
								</c:if>
								<c:if test= "${branch.branchId != user.branch}">
									<option value="${branch.branchId}">${branch.branch}</option>
								</c:if>
							</c:forEach>
						</select><br />

						部署名
						<select class="departments" name="departmentid" id="department">
							<c:forEach items="${departments}" var="department">
								<c:if test= "${department.departmentId == user.department}">
									<option value="${department.departmentId}"selected>${department.department}</option>
								</c:if>
								<c:if test= "${department.departmentId != user.department}">
									<option value="${department.departmentId}">${department.department}</option>
								</c:if>
							</c:forEach>
						</select><br />
					</c:if>

				</div>
				<input type ="hidden" name ="userId" value="${user.id}">
				<input type="submit" value="更新" /> <br />
			</form>

			<div class="copyright"> Copyright(c)JunyaTajiri</div>
		</div>
	</body>
</html>