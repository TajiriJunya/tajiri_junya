<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>掲示板課題</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">

			function disp(){

				// 「OK」時の処理開始 ＋ 確認ダイアログの表示
				if(window.confirm('本当に削除しますか？')){
					location.href = "./";
					return true;
				}
				// 「OK」時の処理終了
				// 「キャンセル」時の処理開始
				else{
					window.alert('キャンセルされました');
					return false;
					// 警告ダイアログを表示
				}
				// 「キャンセル」時の処理終了
				}

		</script>
	</head>

	<body>
		<div class="header">
			<c:if test="${ filterDepartment == 1 && filterBranch == 1}">
				<a href="management">ユーザー管理</a>
			</c:if>
			<a href="logout">ログアウト</a>
			<a href="message">新規投稿</a>
		</div>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="index.jsp" method="get">
			<label for="date">日付指定</label>
			<input type = "date" id= "start" name="start" value = "${start}">
		  ～<input type = "date" id= "end" name="end" value = "${end}"><br/>
			<label for="categoriyName">カテゴリ検索</label>
			<input name="categoryWord" value = "${categoryWord}">
			<input type="submit" value = "絞り込み"><br/>
		</form>

		<div class="main-contents">
			<div class="messages">
				<c:forEach items="${messages}" var="message">

					<div class="message">
						<div class="account-name">
							<span class="name">ユーザー：<c:out value="${message.name}" /></span><br>
							<span class="category">カテゴリ：<c:out value="${message.category}" /></span><br>
							<span class="title">タイトル：<c:out value="${message.title}" /></span><br>
						</div>
						<div class="text"><c:out value="${message.text}" /></div>
						<div class="date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
						<c:if test="${loginUserId == message.userId}">
							<form action="deletemessage" method="post"><br />
								<input type ="hidden" name ="messageId" value="${message.id}">
								<input type="submit" value="投稿削除" onclick="return disp()" >
							</form>
						</c:if>
					</div>

					<div class="comment-erea">
						<c:forEach items="${comments}" var="comment">
							<c:if test="${ comment.messageId == message.id }">
								<span class="name"><c:out value="${comment.userName}" /></span><br />
								<div class="text"><pre>${comment.text}</pre></div><br />
								<div class="date"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
								<c:if test="${ loginUserId == comment.userId || loginUserId == message.userId}">
									<form action="deletecomment" method="post"><br />
										<input type ="hidden" name ="commentId" value="${comment.id }">
										<input type="submit" value="コメント削除" onclick="return disp()">
									</form>
								</c:if>
							</c:if>
						</c:forEach>
					</div>

					<div class="form-area">
						<form action="comment" method="post"><br />
							コメントを入力<br /><textarea name="text" cols="100" rows="5" class="CommentBox"></textarea><br />
							<input type ="hidden" name ="messageId" value="${message.id}">
							<input type="submit" value="コメント">
						</form>
					</div>

				</c:forEach>
			</div>
		</div>

	</body>
</html>