<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
<body>
	<div class="header">
		<a href="./">ホーム</a>
	</div>

	<div class="errorMessages">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>
	</div>

	<div class="form-area">

		<form action="message" method="post">

			<label for="title">件名</label> <br />
			<input name="title" id="title" value="${Retitle}"/> <br />

			<label for="category">カテゴリ</label> <br />
			<input name="category" id="category" value="${Recategory}"/> <br />

			<label for="text">投稿内容</label> <br />
			<textarea name="text" cols="100" rows="5" class="text-box" >${Retext}</textarea>
			<br />
			<input type="submit" value="投稿" >
		</form>

	</div>
</body>
</html>