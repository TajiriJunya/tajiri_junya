<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザ管理</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<script type="text/javascript">

			function disp(){

				// 「OK」時の処理開始 ＋ 確認ダイアログの表示
				if(window.confirm('本当に変更しますか？')){
					location.href = "./";
					return true;
				}
				// 「OK」時の処理終了
				// 「キャンセル」時の処理開始
				else{
					window.alert('キャンセルされました');
					return false;
					// 警告ダイアログを表示
				}
				// 「キャンセル」時の処理終了
				}

		</script>
	</head>
	<body>
		<div class="header">
				<a href="signup">ユーザー新規登録</a>
				<a href="./">ホーム</a>
			</div>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<div class="accounts">
			<c:forEach items="${accounts}" var="account">

				<div class="account">
					<span class="account">アカウント:<c:out value="${account.account}" /> </span><br>
					<span class="name">名前:<c:out value="${account.name}" /></span><br>
					<span class="branch">支社:<c:out value="${account.branch}" /></span><br>
					<span class="department">部署:<c:out value="${account.department}" /></span><br>
					<span class="isStopped">アカウント停止状態<c:out value="${account.isStopped}" /></span><br>
				</div>

				<form action = "setting" method="get">
					<input type ="hidden" name ="userId" value="${account.id}">
					<input type ="submit" value ="編集">
				</form>
				<c:if test="${loginUserId != account.id}">
					<c:if test="${account.isStopped == 0}">
						<form action = "stop" method="post">
							<input type ="hidden" name ="userId" value="${account.id}">
							<input type ="hidden" name ="status" value="1">
							<input type ="submit" value ="停止" onclick="return disp()">
						</form>
					</c:if>
					<c:if test="${account.isStopped == 1}">
						<form action = "stop" method="post">
							<input type ="hidden" name ="userId" value="${account.id}">
							<input type ="hidden" name ="status" value="0">
							<input type ="submit" value ="復活" onclick="return disp()">
						</form>
					</c:if>
				</c:if>
			</c:forEach>

			<div class="copyright">Copyright(c)JunyaTajiri</div>
		</div>
	</body>
</html>
